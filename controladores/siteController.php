<?php
namespace controladores;

class siteController extends Controller{
    private $miPie;
    private $miMenu;
        
    public function __construct() {
        parent::__construct();
        $this->miPie="Prueba de clase.<br>Autor Ramon Abramo";
        // añado el mensaje de la base de datos
        $bd=new \clases\Bbdd();
        $this->miPie.="<br>" . $bd->getMensaje();
        $this->miMenu=[
            "Inicio"=>$this->crearRuta(["accion"=>"index"])                
        ];
    }
    
    /**
     * cuando carga la web
     */
    public function indexAccion(){
      $this->render([
          "vista"=>"index",
          "pie"=>$this->miPie,
          "menu"=>(new \clases\Menu($this->miMenu,"Inicio"))->html()
    ]);
    }
    
    /**
     * cuando pulsa
     */
    public function paso2Accion($p) {
        $resultado=new \clases\Tabla($p->getValores()["numero"]);
        $this->render([
          "vista"=>"paso2",
          "pie"=>$this->miPie,
          "menu"=>(new \clases\Menu($this->miMenu,"Inicio"))->html(),
          "resultado"=>$resultado->dibujar(),  
    ]);
        
    }
    
    public function paso3Accion($p) {
        $resultado=new \clases\Numeros($p->getValores()["numeros"]);
        //almaceno los datos en el archivo salidas.txt
        \clases\Numeros::escribirFichero($resultado->dibujar());
        //$resultado->escribirFichero($resultado->dibujar());
        \clases\Numeros::escribirFichero("Los numeros introducidos: ");
        \clases\Numeros::escribirFichero(join(",",$resultado->getNumeros(false)));
        $this->render([
          "vista"=>"paso3",
          "pie"=>$this->miPie,
          "menu"=>(new \clases\Menu($this->miMenu,"Inicio"))->html(),
          "resultado"=>$resultado->dibujar(),  
    ]);
        
    }
    
}
