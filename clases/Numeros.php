<?php

namespace clases;

class Numeros {
    public $numeros;
    public function __construct($n) {
        $this->numeros=$n;
    }
    /**
     * 
     * @param type $blanco indica si quiero mostrar los vacios o no
     * si es true muestra todo y si es false no
     * @return array 
     */
    public function getNumeros($blanco=true) {
        if(!$blanco){
            $a=[];
            foreach ($this->numeros as $v) {
                if(!empty($v)){
                    $a[]=$v;  
                }
            }
            return $a;
        }
        return $this->numeros;
    }

    public function setNumeros($numeros) {
        
        // logica de control o negocio
        $this->numeros = $numeros;
        return $this;
    }
    
    private function repetidos(){
        $s=array_count_values($this->getNumeros(false));
        if(max($s)>1){
            return true;
        }else{
            return false;
        }
    }
    
    private function llenos(){
        return count($this->getNumeros(false));
    }
    
    
    public function dibujar(){
        // mensaje 1
        if($this->repetidos()){
            $mensaje="Hay elementos repetidos.";
        }else{
            $mensaje="No hay elementos repetidos.";
        }
        
        //mensaje 2
        $mensaje.="<BR>Has rellenado  " . 
                $this->llenos() .
                " cajas de un total de " .
                count($this->getNumeros());
        
        return $mensaje;
    }
    
    public static function escribirFichero($texto,$nombre="./logs/salidas.txt"){
        file_put_contents($nombre, "\n" . $texto,FILE_APPEND);
    }
    
    
    
    


    
}
