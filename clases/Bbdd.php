<?php


namespace clases;

class Bbdd {
    private $conexion;
    
    public function __construct() {
        $this->conexion=new \mysqli("127.0.0.1","root","","prueba1PHP");
        $this->conexion->query("SET NAMES 'utf8'");
    }
    
    public function consulta($c="SELECT * FROM mensajes"){
        return $this->conexion->query($c)->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getMensaje(){
        return $this->consulta()[random_int(0, 5)]["texto"];
    }
}
